<html>
<head>
	<title>Urbandale Water Utility</title>
	<link rel = "stylesheet" type = "text/css" href = "<?php echo get_bloginfo('template_url'); ?>/css/bootstrap.min.css" media = "screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/style.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/vendor.css" media="screen" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php wp_head(); ?>
</head>
<body>

		<div class = "banner special">

			<div class = "container special">
				<p class = "special_text"> Special Alert goes Here. </p>
				<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

		</div>

		<div class = "banner title">

			<div class = "container title">
				
				<div class ="row">
					<div class = "col-md-9 title_and_logo">
						<a href = "<?php echo get_home_url(); ?>"><img src = "<?php echo get_bloginfo('template_url') ?>/images/title.png" class = "title_logo"/></a>
					</div>

					<div class ="col-md-3 phone_and_logo">
						<img src = "<?php echo get_bloginfo('template_url') ?>/images/phone.png" class = "phone_logo">
						<p class = "phone_number">(515) 278 3940</p>
					</div>
				</div>

			</div>

		</div>

		<div class = "banner nav">

			<div class = "container nav">
					<h4 class = "menu_title">Menu</h4>
					<img src = "<?php echo get_bloginfo('template_url'); ?>/images/menu.png" class = "more_menu">
					<?php wp_nav_menu( array( 'theme-location' => 'main-menu', 'container' => '') ); ?>
					<button type = "button" class = "btn btn-primary pay_bill" role = "button">Pay My Bill</button>
			</div>

		</div>
