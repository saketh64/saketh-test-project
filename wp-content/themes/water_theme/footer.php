	<div class = "banner footer">

		<div class = "container footer">
			
			<div class = "row">

				<div class = "col-md-4">
					<h4 class = "footer_title 1">Quick Links</h4>
					<ul class = "list footer_content 1">
						<li>City of Urbandale</li>
						<hr class = "line one">
						<li>Des Moines Water Works</li>
						<hr class = "line two">
						<li>Metro Waste Authority - Urbandale</li>
						<hr class = "line three">
						<li>BSI  - Online Backflow Tracking</li>
						<hr class = "line four">
						<li>American WaterWorks Assosciation - IA</li>
						<hr class = "line five">
						<li>Iowa One Call</li>
						<hr class = "line six">
					</ul>
				</div>

				<div class = "col-md-4">
					<h4 class = "footer_title 2">Hours of Operation</h4>
					<ul class = "list footer_content 2">
						<li>Monday - Friday</li>
						<li>8:00 - 4:30pm</li>
						<hr class = "line nine">
					</ul>
				</div>

				<div class = "col-md-4">
					<h4 class = "footer_title 3">Contact</h4>
					<ul class = "list footer_content 3">
						<li>
							<img src = "<?php echo get_bloginfo('template_url') ?>/images/location.png" class = "icon location" />
							<p class = "contact_text 1">Urbandale Water Utility<br>
							3720 86th Street<br>
							Urbandale, IA 50322 
							</p>
							<hr class = "line seven">
						</li>
						<li>
							<img src = "<?php echo get_bloginfo('template_url') ?>/images/cell.png" class = "icon cell" />
							<p class = "contact_text 2">(515) 278 3940</p>
							<hr class = "line eight">
						</li>
						<li>
							<img src = "<?php echo get_bloginfo('template_url') ?>/images/mail.png" class = "icon mail" />
							<p class ="contact_text 3">customerservice@urbandalewater.org</p>
						</li>
					</ul>
				</div>

			</div> <!-- row -->

			<hr>

			<div class = "copyright_section">
				<img src = "<?php echo get_bloginfo('template_url') ?>/images/copyright.jpg" class = "copyright_logo" />
				<p class = "copyright_text">2015 Urbandale Water Utility</p>
			</div>
			<p class = "webspec_text">Iowa Web Design by WebSpec Design</p>

		</div><!-- container -->

	</div><!-- banner -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type = "text/javascript" src="<?php bloginfo("template_url"); ?>/js/script.js"></script>

<script type = "text/javascript" src = "<?php echo get_bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>

<?php wp_footer(); ?>
</body>
</html>