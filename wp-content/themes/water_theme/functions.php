<?php
function theme_header_scripts()
{
	wp_register_style('theme_vendor', get_template_directory_uri() . '/vendor.css', array());
    wp_enqueue_style('theme_vendor');

	wp_register_style('theme_style', get_template_directory_uri() . '/style.css', array());
    wp_enqueue_style('theme_style');
}
add_action('wp_enqueue_scripts', 'theme_header_scripts');
function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );
?>