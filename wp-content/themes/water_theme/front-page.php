<?php get_header(); ?>
<div class = "banner front_pic">
	
	<img src = "<?php echo get_bloginfo('template_url') ?>/images/water_building_pic.jpg" class = "front_page_pic img-responsive" />
	
	<div class = "photo_caption_box">
		
		<h3><?php the_field('photo_heading'); ?></h3>
		<br>
		<p><?php the_field('photo_caption'); ?></p>
		<br>
		<button type = "button" class ="btn btn-default photo_link" role = "button">Link</button>
	</div> 

</div>

<div class = "banner company_description">
	<div class = "container company_description">

		<div class = "row">
			<div class = "col-md-8">
				<h3>Urbandale Water Utility</h3>
				<br>
				<p><?php the_field('company_description'); ?></p>
			</div>
		</div>

	</div>
</div>

<div class = "banner services">
	<div class = "container services">
		
		<div class = "row">
			<div class = "col-md-8 services_title">
				<h3 class = "blue_heading">Services of Urbandale Water Utility</h3>
				<br>
				<p><?php the_field('services_description'); ?></p>
			</div>

			<div class = "col-md-4">
				<img src = "<?php echo get_bloginfo('template_url') ?>/images/water_droplets.jpg" class = "img-rounded img-responsive" />
				<button type = "button" class = "btn btn-primary btn-block" role = "button">Direct Pay Sign Up</button>
			</div>
		</div>

	</div>
</div>

<div class = "banner projects">
	<div class = "container projects">

		<h3 class = "blue_heading">Current Project Updates</h3>
		<br>
		<p>Stay up to date with our new projects and infrastructure updates.</p>

		<div class = "row">

			<div class = "col-md-4">
				<h5 class = "blue_heading"><?php the_field('project_title_1'); ?></h5>
				<div class = "project_box one">
				</div>
				<p><?php the_field('project_description_1'); ?></p>
			</div>

			<div class = "col-md-4">
				<h5 class = "blue_heading"><?php the_field('project_title_2'); ?></h5>
				<div class = "project_box two">
				</div>
				<p><?php the_field('project_description_2'); ?></p>
				</p>
			</div>

			<div class = "col-md-4">
				<h5 class = "blue_heading"><?php the_field('project_title_3'); ?></h5>
				<div class = "project_box three">
				</div>
				<p><?php the_field('project_description_3'); ?></p>
				</p>
			</div>

		</div>

	</div>
</div>

			


<?php get_footer(); ?>