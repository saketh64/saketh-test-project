<style>
/* Template Name: Page */
</style>

<?php get_header(); ?>

<div class = "banner page_title">

	<div class = "container page_title">

		<h2 class = "page_title_text"><?php the_title(); ?></h2>



	</div>

</div>

<div class = "banner page_content">

	<div class = "container page_content">

		<p class = "page_content_text">
			<?php if (have_posts()) :
				while (have_posts()) : the_post() ;
					the_content();
				endwhile;
			endif; ?>
		</p>

	</div>

</div>

<?php get_footer(); ?>